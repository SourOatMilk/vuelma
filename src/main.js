window._ = require('lodash')

import Vue from 'vue'
import App from './App'
import store from './store'
import Vuelma from './index'

const customConfig = {
  dropdown: {
    hoverEnter: 50
  }
}

Vue.use(Vuelma, { store }, customConfig)

new Vue({ // eslint-disable-line no-new
  el: '#app',
  store,
  render: (h) => h(App)
})
