import { vbstore } from './store/index'

let Vuelma = {}

const importAll = (Vue, r) => {
  r.keys().forEach(key => {
    Vue.component(r(key).name, r(key))
  })
}

const defaultConf = {
  dropdown: {
    hoverEnter: 100,
    hoverExit: 500
  },

  pagination: {
    startFromOne: false
  }
}

Vuelma.install = (Vue, { store }, customConf = {}) => {
  if (!store) {
    throw new Error('Provide Vuex store, please!')
  }

  const conf = {...defaultConf}

  Object.keys(customConf).forEach(key => {
    console.log(key, defaultConf[key], customConf[key])
    conf[key] = Object.assign(defaultConf[key], customConf[key])
  })

  Vue.mixin({vuelma: conf})
  Vue.prototype.$vuelma = conf

  store.registerModule('vuelma', vbstore)

  importAll(Vue, require.context('./components/', true, /\.vue$/))
}

export default Vuelma
