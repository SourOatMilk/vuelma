export default {
  openModal: (state, name) => {
    let obj = _.cloneDeep(state.instances)
    let modal = obj.Modal
    _.each(modal, (instance) => {
      instance.active = instance.name === name
    })

    state.instances = obj
  },

  closeModal: (state) => {
    let obj = _.cloneDeep(state.instances)
    let modal = obj.Modal
    _.each(modal, (instance) => {
      instance.active = false
    })

    state.instances = obj
  },

  createNotification: (state, data) => {
    let obj = _.cloneDeep(state.instances)
    let notifications = obj.Notifications
    let instance = _.find(notifications, { name: data.name })
    let uid = JSON.stringify(data).length.toString(2)
    let timestamp = _.now()

    data.notification.timestamp = timestamp
    data.notification.uid = parseInt(uid) & timestamp + parseInt(uid)

    instance.items.push(data.notification)

    state.instances = obj
  },

  destroyNotification: (state, data) => {
    let obj = _.cloneDeep(state.instances)
    let instance = obj.Notifications[data.id]
    let items = instance.items
    let n = _.find(items, { timestamp: data.timestamp })
    let i = _.indexOf(items, n)

    state.instances.Notifications[data.id].items.splice(i, 1)
  },

  setActiveForInstance: (state, instance) => {
    let type = instance.type
    let id = instance.id
    let data = instance.data

    let obj = _.cloneDeep(state.instances)
    obj[type][id].active = data

    state.instances = obj
  },

  setDataForInstance: (state, instance) => {
    let type = instance.type
    let id = instance.id
    let data = instance.data

    let obj = _.cloneDeep(state.instances)

    _.merge(obj[type][id], data)

    state.instances = obj
  },

  registerInstance: (state, instance) => {
    let type = instance.type
    let id = instance.id
    let data = instance.data
    let obj = _.cloneDeep(state.instances) || {}

    obj[type] = obj[type] || {}
    obj[type][id] = data

    state.instances = obj
  },

  unregisterInstance: (state, instance) => {
    delete state[instance.type][instance.id]
  }
}
