export default {
  getInstances: state => {
    return state.instances
  },

  getNotifications: state => {
    return state.instances.Notifications
  },

  // TODO: Instances should use the same getter if possible
  getPageFromPagination: (state) => (name) => {
    let i = _.find(state.instances.Pagination, { name: name })
    return i && i.currentPage ? i.currentPage : 0
  },

  getActiveFromInstance: (state) => (instance, name, tab) => {
    let i = _.find(state.instances[instance], { name: name })
    let isActive = i && i.active && i.active.name === tab
    return isActive
  }
}

