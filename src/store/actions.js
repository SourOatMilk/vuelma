export default {
  test: (store, payload) => {
    console.log('it works', payload)
  },

  triggerAction: ({dispatch}, action) => {
    dispatch(action.type, action.data)
  },

  openModal: ({commit}, name) => {
    commit('openModal', name)
  },

  closeModal: ({commit}) => {
    commit('closeModal')
  },

  createNotification: ({commit}, data) => {
    commit('createNotification', data)
  },

  destroyNotification: ({commit}, data) => {
    commit('destroyNotification', data)
  },

  setActiveForInstance: ({commit}, instance) => {
    commit('setActiveForInstance', instance)
  },

  setDataForInstance: ({state, commit}, instance) => {
    let instances = state.instances[instance.type]
    let exists = _.find(instances, { name: instance.data.name })
    if (exists !== undefined) {
      instance.data.name = `${instance.data.name}-${instance.id}`
      console.warn(
        `Instance name should be unique: '${exists.name}'. Renamed to '${instance.data.name}'`
      )
    }
    commit('setDataForInstance', instance)
  },

  registerInstance: ({commit}, instance) => {
    commit('registerInstance', instance)
  },

  unregisterInstance: ({commit}, instance) => {
    commit('unregisterInstance', instance)
  }
}
