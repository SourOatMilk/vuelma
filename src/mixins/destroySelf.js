export default {
  data () {
    return {
      isDestroyed: false
    }
  },

  methods: {
    destroySelf () {
      this.isDestroyed = true

      _.defer(() => {
        this.$destroy()
      })
    }
  }
}
