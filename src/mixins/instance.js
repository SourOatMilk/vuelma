import { mapActions } from 'vuex'

export default {
  data () {
    return {
      _instance: {},
      initialInstanceData: null,
      collectedChildrens: []
    }
  },

  mounted () {
    this._instance = {
      type: this.$options.name,
      id: this._uid,
      data: this.initialInstanceData
    }

    this.registerInstance(this._instance)

    this.collectChildrens()
  },

  beforeDestroyed () {
    this.unregisterInstance({
      type: this._instance.type,
      id: this._instance.id
    })
  },

  methods: {
    ...mapActions({
      registerInstance: 'vuelma/registerInstance',
      unregisterInstance: 'vuelma/unregisterInstance',
      setDataForInstance: 'vuelma/setDataForInstance',
      setActiveForInstance: 'vuelma/setActiveForInstance'
    }),

    setAsParentInstance (children) {
      children._parentInstance = this
    },

    collectChildrens (x = this) {
      let childrens = x.$children

      if (childrens) {
        childrens.forEach((c) => {
          this.collectedChildrens.push(c)
          this.setAsParentInstance(c)
          this.collectChildrens(c)
        })
      }
    }
  }
}
