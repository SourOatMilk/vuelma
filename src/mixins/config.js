export default {
  methods: {
    getConf () {
      const n = this.$options.name[0].toLowerCase() + this.$options.name.substr(1)
      return this.$root.$vuelma[n]
    }
  }
}
