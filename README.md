# Vuelma

> Vue + Bulma = Vuelma

## WARNING

This is absolutely not yet ready for production! ... But feel free to test it out and all that.

## Install
In your `main.js`:
```
import Vue from 'vue'
import store from './store'
import Vuelma from 'vuelma'

Vue.use(Vuelma, { store })

new Vue({
  // Setup
})

```

## Usage

TODO


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
