import Vue from 'vue'
import store from '@/store'
import { vbstore } from '@/store/index'
import C from '@/components/components/pagination/Pagination'

describe('Pagination.vue', () => {
  describe('Data', () => {
    it('is a function', () => {
      expect(typeof C.data).toBe('function')
    })

    it('has no currentPage', () => {
      const defaultData = C.data()
      expect(defaultData.currentPage).toBe(null)
    })

    it('has empty object for initialInstanceData', () => {
      const defaultData = C.data()
      expect(typeof defaultData.initialInstanceData).toBe('object')
      expect(defaultData.initialInstanceData).toEmpty
    })
  })

  describe('Props', () => {
    const defaultProps = C.props

    it('is an object', () => {
      expect(typeof defaultProps).toBe('object')
    })

    it('has a name', () => {
      expect(defaultProps.name.type).toBe(String)
      expect(defaultProps.name.default).toBe('default-pagination')
    })

    it('has pagesTotal 8', () => {
      expect(defaultProps.pagesTotal.type).toBe(Number)
      expect(defaultProps.pagesTotal.default).toEqual(8)
    })

    it('has showMax 5', () => {
      expect(defaultProps.showMax.type).toBe(Number)
      expect(defaultProps.showMax.default).toEqual(5)
    })

    it('has startFrom 0', () => {
      expect(defaultProps.startFrom.type).toBe(Number)
      expect(defaultProps.startFrom.default).toEqual(0)
    })

    it('has next text', () => {
      expect(defaultProps.next.type).toBe(String)
      expect(defaultProps.next.default).toBe('Next')
    })

    it('has previous text', () => {
      expect(defaultProps.previous.type).toBe(String)
      expect(defaultProps.previous.default).toBe('Previous')
    })

    it('has indexingStartsFromOneBecauseIamBeingWeird set to false', () => {
      const n = 'indexingStartsFromOneBecauseIamBeingWeird'
      expect(defaultProps[n].type).toBe(Boolean)
      expect(defaultProps[n].default).toEqual(false)
    })
  })

  it('Pagination mounted and defaults work!', () => {
    // FIXME: There must be a better way to make sure module is registered only once
    if (!store._modulesNamespaceMap['vuelma/']) {
      store.registerModule('vuelma', vbstore)
    }
    const Constructor = Vue.extend(C)
    const vm = new Constructor({ store }).$mount()
    expect(vm.$children.length).toBe(7)
  })
})

