import Vue from 'vue'
import store from '@/store'
import { vbstore } from '@/store/index'
import C from '@/components/components/tabs/Tabs'

describe('Tabs.vue', () => {
  describe('Data', () => {
    it('is a function', () => {
      expect(typeof C.data).toBe('function')
    })

    it('has no active', () => {
      const defaultData = C.data()
      expect(defaultData.active).toBe(null)
    })
  })

  describe('Props', () => {
    const defaultProps = C.props

    it('is an object', () => {
      expect(typeof defaultProps).toBe('object')
    })

    it('has a name', () => {
      expect(defaultProps.name.type).toBe(String)
      expect(defaultProps.name.default).toBe('default-tabs')
    })

    it('has useSlot false', () => {
      expect(defaultProps.useSlot.type).toBe(Boolean)
      expect(defaultProps.useSlot.default).toBe(false)
    })

    it('has default items', () => {
      expect(defaultProps.items.type).toBe(Array)
      expect(typeof defaultProps.items.default).toBe('function')
      expect(defaultProps.items.default()).toEqual([
        {name: 'tab-a', text: 'Tab A', href: '#', active: true},
        {name: 'tab-b', text: 'Tab B', icons: {left: 'times'}},
        {name: 'tab-c', text: 'Tab C', icons: {right: 'trash-o'}}
      ])
    })
  })

  it('Tabs mounted and defaults work!', () => {
    // FIXME: There must be a better way to make sure module is registered only once
    if (!store._modulesNamespaceMap['vuelma/']) {
      store.registerModule('vuelma', vbstore)
    }
    const Constructor = Vue.extend(C)
    const vm = new Constructor({ store }).$mount()
    expect(vm.$children[0].$options.name).toEqual('Tab')
    expect(vm.$children.length).toEqual(3)
    expect(vm.active.name).toEqual('tab-a')
  })
})
